import { Component, HostListener, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { MatSnackBar } from "@angular/material";
import { Column } from './components/table-sheets/interfaces';
import { CellType } from './components/table-sheets/shared/cell-type.enum';

// interface MouseEvent {
//   rowId: number;
//   colId: number;
// }

// interface PeriodicElement {
//     name: string;
//     position: number;
//     weight: number;
//     symbol: string;
// }

// interface SelectedCellCoordination {
//     x: number;
//     y: number;
// }

// enum CellStatus {
//     Unselect,
//     Fitting,
//     Selected,
//     Pointer,
//     Editing
// }

const columns: Column[] = [
    {
        name: 'position',
        isEditable: true,
        dataType: CellType.Number
    },
    {
        name: 'name',
        isEditable: false,
        dataType: CellType.Text
    },
    {
        name: 'weight',
        isEditable: true,
        dataType: CellType.Money
    },
    {
        name: 'symbol',
        isEditable: true,
        dataType: CellType.Text
    },
    {
        name: 'kek',
        isEditable: true,
        dataType: CellType.Text
    },
    {
        name: 'pizda',
        isEditable: true,
        dataType: CellType.Text
    }
]

const ELEMENT_DATA: {}[] = [
    {position: 1 , name: 'Hydrogen' , weight: 1.0079 , symbol: 'H', kek: 'hui', pizda: 'oru'},
    {position: 2 , name: 'Helium'   , weight: 4.0026 , symbol: 'He', kek: 'hui', pizda: 'oru'},
    {position: 3 , name: 'Lithium'  , weight: 6.941  , symbol: 'Li', kek: 'hui', pizda: 'oru'},
    {position: 4 , name: 'Beryllium', weight: 9.0122 , symbol: 'Be', kek: 'hui', pizda: 'oru'},
    {position: 5 , name: 'Boron'    , weight: 10.811 , symbol: 'B' , kek: 'hui', pizda: 'oru'},
    {position: 6 , name: 'Carbon'   , weight: 12.0107, symbol: 'C' , kek: 'hui', pizda: 'oru'},
    {position: 7 , name: 'Nitrogen' , weight: 14.0067, symbol: 'N' , kek: 'hui', pizda: 'oru'},
    {position: 8 , name: 'Oxygen'   , weight: 15.9994, symbol: 'O' , kek: 'hui', pizda: 'oru'},
    {position: 9 , name: 'Fluorine' , weight: 18.9984, symbol: 'F' , kek: 'hui', pizda: 'oru'},
    {position: 10, name: 'Neon'     , weight: 20.1797, symbol: 'Ne', kek: 'hui', pizda: 'oru'},
];

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {

    public ELEMENT_DATA = ELEMENT_DATA;
    public columns = columns;

    // public displayedColumns: string[];
    // public dataSource = ELEMENT_DATA;

    // public tableMouseDown: MouseEvent;
    // public tableMouseUp: MouseEvent;
    // public tableMouseOver: MouseEvent;

    // public FIRST_EDITABLE_ROW: number = 0;
    // public LAST_EDITABLE_ROW: number = ELEMENT_DATA.length - 1; // = 9
    // public FIRST_EDITABLE_COL: number = 0;
    // public LAST_EDITABLE_COL: number; // = 3

    // public CellStatus = CellStatus;

    // public pointerCell: SelectedCellCoordination;
    // public selectedCellsState: CellStatus[][];

    // public isEditingCell: boolean = false;

    // constructor (public snackBar: MatSnackBar) {}

    // public ngOnInit (): void {
    //     this.initDisplayedColumns();
    //     this.initSelectedCellsState(ELEMENT_DATA.length, this.displayedColumns.length);

    //     this.LAST_EDITABLE_COL = this.displayedColumns.length - 1;
    // }

    // public updateSelectedCellsValues (text: string): void {

    //     if (text == null) { return; }

    //     if (this.tableMouseDown && this.tableMouseUp) {
    //             const dataCopy: {}[] = this.dataSource.slice(); // copy and mutate

    //             const [from, to] = this.getFromToByMouseEvent(this.tableMouseDown, this.tableMouseUp);

    //             if (from.y === to.y) {
    //                 console.log('--Edit cells from the same column');
    //                 for (let i = from.x; i <= to.x; i++) {
    //                     dataCopy[i][this.displayedColumns[from.y]] = text;
    //                 }
    //             } else {
    //                 console.log('--Edit cells starting and ending not on the same column');

    //                 for (let i = from.x; i <= to.x; i++) {
    //                     for (let j = from.y; j <= to.y; j++) {
    //                         dataCopy[i][this.displayedColumns[j]] = text;
    //                     }
    //                 }
    //             }
    //             console.log('--update: ' + JSON.stringify(from) + ', ' + ' to ' + JSON.stringify(to));
    //             this.dataSource = dataCopy;
    //     }
    // }


    // public onMouseDown (rowId: number, colId: number): void {
    //     if (this.isEditingCell) {
    //         if (this.pointerCell.x === colId && this.pointerCell.y === rowId) {
    //             return;
    //         }
    //     }

    //     this.tableMouseDown = {
    //         rowId,
    //         colId,
    //         // cellsType
    //     };

    //     this.pointerCell = {
    //         x: colId,
    //         y: rowId
    //     };

    //     this.unselectAll();
    //     this.addNewPointer(this.pointerCell);
    //     this.isEditingCell = false;
    // }

    // public onMouseUp (rowId: number, colId: number): void {
    //     this.tableMouseUp = {
    //         rowId,
    //         colId,
    //     };

    //     if (this.tableMouseDown) {
    //         this.updateSelectedCellsState(this.tableMouseDown, this.tableMouseUp);

    //         this.tableMouseDown = null;
    //     }
    // }

    // public onMouseOver (rowId: number, colId: number): void {
    //     this.tableMouseOver = {
    //         rowId,
    //         colId
    //     };

    //     if (this.tableMouseDown) {
    //         this.updateFittingCellsState(this.tableMouseDown, this.tableMouseOver);
    //     }
    // }

    // public onMouseDBLClick (): void {
    //     this.enableEditingPointigCell();
    // }

    // @HostListener('document:keydown', ['$event'])
    // public onKeyUp (event: KeyboardEvent): void {
    //     switch (event.code) {
    //         case 'ArrowUp':
    //             if (this.isEditingCell) { return; }
    //             this.upPointer();
    //             break;
    //         case 'ArrowDown':
    //             if (this.isEditingCell) { return; }
    //             this.downPointer();
    //             break;
    //         case 'ArrowLeft':
    //             if (this.isEditingCell) { return; }
    //             this.leftPointer();
    //             break;
    //         case 'ArrowRight':
    //             if (this.isEditingCell) { return; }
    //             this.rightPointer();
    //             break;
    //         case 'Delete':
    //             this.clearDataInSelectedCells();
    //             break;
    //         case 'Backspace':
    //             this.clearDataInSelectedCells();
    //             break;
    //         case 'Escape':
    //             this.onEscapeTap();
    //             break;
    //         case 'Enter':
    //             this.enableEditingPointigCell();
    //             break;
    //     }
    // }

    // private initDisplayedColumns (): void {
    //     this.displayedColumns = Object.keys(ELEMENT_DATA[0]);
    // }

    // private initSelectedCellsState (x: number, y: number): void {
    //     this.selectedCellsState = Array.from({ length: x },
    //         () => (
    //             Array.from({ length: y }, () => CellStatus.Unselect)
    //         )
    //     );
    // }

    // private updateSelectedCellsState (mouseDown: MouseEvent, mouseUp: MouseEvent): void {
    //     const [from, to] = this.getFromToByMouseEvent(mouseDown, mouseUp);

    //     this.unselectAll();

    //     this.selectFromTo(
    //         from as SelectedCellCoordination,
    //         to as SelectedCellCoordination
    //     );
    // }

    // private updateFittingCellsState (mouseDown: MouseEvent, mouseUp: MouseEvent): void {
    //     const [from, to] = this.getFromToByMouseEvent(mouseDown, mouseUp);

    //     this.unselectAll();

    //     this.fittingFromTo(
    //         from as SelectedCellCoordination,
    //         to as SelectedCellCoordination
    //     );
    // }

    // private selectFromTo (from: SelectedCellCoordination, to: SelectedCellCoordination): void {
    //     for (let i = from.x; i <= to.x; i++) {
    //         for (let j = from.y; j <= to.y; j++) {
    //             this.selectedCellsState[j][i] = CellStatus.Selected;
    //         }
    //     }
    //     this.addNewPointer(this.pointerCell);
    // }

    // private fittingFromTo (from: SelectedCellCoordination, to: SelectedCellCoordination): void {
    //     for (let i = from.x; i <= to.x; i++) {
    //         for (let j = from.y; j <= to.y; j++) {
    //             this.selectedCellsState[j][i] = CellStatus.Fitting;
    //         }
    //     }

    //     this.addNewPointer(this.pointerCell);
    // }

    // private unselectAll (): void {
    //     for (let i = this.FIRST_EDITABLE_ROW; i <= this.LAST_EDITABLE_ROW; i++) {
    //         for (let j = this.FIRST_EDITABLE_COL; j <= this.LAST_EDITABLE_COL; j++) {
    //             this.selectedCellsState[i][j] = CellStatus.Unselect;
    //         }
    //     }
    // }

    // private addNewPointer (newPointer: SelectedCellCoordination): void {
    //     this.selectedCellsState[newPointer.y][newPointer.x] = CellStatus.Pointer;
    // }

    // private getFromToByMouseEvent (mouseUp: MouseEvent, mouseDown: MouseEvent): [SelectedCellCoordination, SelectedCellCoordination] {
    //     // tslint:disable-next-line: prefer-const
    //     let from: Partial<SelectedCellCoordination> = {};
    //     // tslint:disable-next-line: prefer-const
    //     let to: Partial<SelectedCellCoordination> = {};

    //     if (mouseDown.colId <= mouseUp.colId) {
    //         from.x = mouseDown.colId;
    //         to.x = mouseUp.colId;
    //     } else {
    //         from.x = mouseUp.colId;
    //         to.x = mouseDown.colId;
    //     }

    //     if (mouseDown.rowId <= mouseUp.rowId) {
    //         from.y = mouseDown.rowId;
    //         to.y = mouseUp.rowId;
    //     } else {
    //         from.y = mouseUp.rowId;
    //         to.y = mouseDown.rowId;
    //     }

    //     return [from as SelectedCellCoordination, to as SelectedCellCoordination];
    // }

    // private enableEditingPointigCell (): void {
    //     if (this.pointerCell) {
    //         this.selectedCellsState[this.pointerCell.y][this.pointerCell.x] = CellStatus.Editing;
    //         this.isEditingCell = true;
    //     }
    // }

    // private upPointer (): void {
    //     this.pointerCell.y = (this.pointerCell.y > 0) ? this.pointerCell.y - 1 : this.pointerCell.y;

    //     this.unselectAll();
    //     this.addNewPointer(this.pointerCell);
    // }

    // private downPointer (): void {
    //     this.pointerCell.y = (this.pointerCell.y < this.LAST_EDITABLE_ROW) ? this.pointerCell.y + 1 : this.pointerCell.y;
    //     this.unselectAll();
    //     this.addNewPointer(this.pointerCell);
    // }

    // private leftPointer (): void {
    //     this.pointerCell.x = (this.pointerCell.x > 0) ? this.pointerCell.x - 1 : this.pointerCell.x;
    //     this.unselectAll();
    //     this.addNewPointer(this.pointerCell);
    // }

    // private rightPointer (): void {
    //     this.pointerCell.x = (this.pointerCell.x < this.LAST_EDITABLE_COL) ? this.pointerCell.x + 1 : this.pointerCell.x;
    //     this.unselectAll();
    //     this.addNewPointer(this.pointerCell);
    // }

    // private onEscapeTap (): void {
    //     this.selectedCellsState[this.pointerCell.y][this.pointerCell.x] = CellStatus.Pointer;
    //     this.isEditingCell = false;
    // }

    // private clearDataInSelectedCells (): void {
    //     for (let i = 0; i < this.dataSource.length; i++) {
    //         for (let j = 0; j < this.displayedColumns.length; j++) {
    //             if (
    //                 this.selectedCellsState[i][j] === CellStatus.Selected ||
    //                 this.selectedCellsState[i][j] === CellStatus.Pointer
    //             ) {
    //                 this.dataSource[i][this.displayedColumns[j]] = '';
    //             }
    //         }
    //     }
    // }
}
