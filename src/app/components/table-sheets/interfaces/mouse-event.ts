export interface MouseEvent {
    rowId: number;
    colId: number;
}
