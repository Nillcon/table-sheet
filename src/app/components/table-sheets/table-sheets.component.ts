import { Component, OnInit, Input, HostListener } from '@angular/core';

import { CellStatus, CellType } from './shared';
import { MouseEvent, SelectedCellCoordination, Column } from './interfaces';
import { MatSnackBar } from '@angular/material';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'table-sheets',
  templateUrl: './table-sheets.component.html',
  styleUrls: ['./table-sheets.component.scss']
})
export class TableSheetsComponent implements OnInit {

    @Input()
    public dataSource: {}[];

    @Input()
    public columns: Column[];

    @Input()
    public disableEditingMessage: string;

    public displayedColumns: string[];

    public CellType = CellType;

    public tableMouseDown: MouseEvent;
    public tableMouseUp: MouseEvent;
    public tableMouseOver: MouseEvent;

    public FIRST_EDITABLE_ROW: number = 0;
    public LAST_EDITABLE_ROW: number; // = 9
    public FIRST_EDITABLE_COL: number = 0;
    public LAST_EDITABLE_COL: number; // = 3

    public CellStatus = CellStatus;

    public pointerCell: SelectedCellCoordination;
    public selectedCellsState: CellStatus[][];

    public isEditingCell: boolean = false;

    public columnWidth: number = 100;

    constructor (
        private readonly _snackBar: MatSnackBar
    ) {}

    public ngOnInit (): void {
        this.initColumnWidth();

        this.initDisplayedColumns();
        this.initSelectedCellsState(this.dataSource.length, this.displayedColumns.length);

        this.LAST_EDITABLE_COL = this.displayedColumns.length - 1;
        this.LAST_EDITABLE_ROW = this.dataSource.length - 1;
    }

    public updateSelectedCellsValues (text: string): void {

        if (text == null) { return; }

        if (this.tableMouseDown && this.tableMouseUp) {
                const dataCopy: {}[] = this.dataSource.slice(); // copy and mutate

                const [from, to] = this.getFromToByMouseEvent(this.tableMouseDown, this.tableMouseUp);

                if (from.y === to.y) {
                    console.log('--Edit cells from the same column');
                    for (let i = from.x; i <= to.x; i++) {
                        dataCopy[i][this.displayedColumns[from.y]] = text;
                    }
                } else {
                    console.log('--Edit cells starting and ending not on the same column');

                    for (let i = from.x; i <= to.x; i++) {
                        for (let j = from.y; j <= to.y; j++) {
                            dataCopy[i][this.displayedColumns[j]] = text;
                        }
                    }
                }
                console.log('--update: ' + JSON.stringify(from) + ', ' + ' to ' + JSON.stringify(to));
                this.dataSource = dataCopy;
        }
    }


    public onMouseDown (rowId: number, colId: number): void {
        if (this.isEditingCell) {
            if (this.pointerCell.x === colId && this.pointerCell.y === rowId) {
                return;
            }
        }

        this.tableMouseDown = {
            rowId,
            colId,
            // cellsType
        };

        this.pointerCell = {
            x: colId,
            y: rowId
        };

        this.unselectAll();
        this.addNewPointer(this.pointerCell);
        this.isEditingCell = false;
    }

    public onMouseUp (rowId: number, colId: number): void {
        this.tableMouseUp = {
            rowId,
            colId,
        };

        if (this.tableMouseDown) {
            this.updateSelectedCellsState(this.tableMouseDown, this.tableMouseUp);

            this.tableMouseDown = null;
        }
    }

    public onMouseOver (rowId: number, colId: number): void {
        this.tableMouseOver = {
            rowId,
            colId
        };

        if (this.tableMouseDown) {
            this.updateFittingCellsState(this.tableMouseDown, this.tableMouseOver);
        }
    }

    public onMouseDBLClick (): void {
        this.enableEditingPointigCell();
    }

    @HostListener('document:keydown', ['$event'])
    public onKeyUp (event: KeyboardEvent): void {
        switch (event.code) {
            case 'ArrowUp':
                if (this.isEditingCell) { return; }
                this.upPointer();
                break;
            case 'ArrowDown':
                if (this.isEditingCell) { return; }
                this.downPointer();
                break;
            case 'ArrowLeft':
                if (this.isEditingCell) { return; }
                this.leftPointer();
                break;
            case 'ArrowRight':
                if (this.isEditingCell) { return; }
                this.rightPointer();
                break;
            case 'Delete':
                if (this.isEditingCell) { return; }
                this.clearDataInSelectedCells();
                break;
            case 'Backspace':
                if (this.isEditingCell) { return; }
                this.clearDataInSelectedCells();
                break;
            case 'Escape':
                this.onEscapeTap();
                break;
            case 'Enter':
                if (this.isEditingCell) { return; }
                this.enableEditingPointigCell();
                break;
        }
    }

    private initDisplayedColumns (): void {
        this.displayedColumns = this.columns.map(column => column.name)
    }

    private initSelectedCellsState (x: number, y: number): void {
        this.selectedCellsState = Array.from({ length: x },
            () => (
                Array.from({ length: y }, () => CellStatus.Unselect)
            )
        );
    }

    private initColumnWidth (): void {
        this.columnWidth = 100 / this.columns.length;
    }

    private updateSelectedCellsState (mouseDown: MouseEvent, mouseUp: MouseEvent): void {
        const [from, to] = this.getFromToByMouseEvent(mouseDown, mouseUp);

        this.unselectAll();

        this.selectFromTo(
            from as SelectedCellCoordination,
            to as SelectedCellCoordination
        );
    }

    private updateFittingCellsState (mouseDown: MouseEvent, mouseUp: MouseEvent): void {
        const [from, to] = this.getFromToByMouseEvent(mouseDown, mouseUp);

        this.unselectAll();

        this.fittingFromTo(
            from as SelectedCellCoordination,
            to as SelectedCellCoordination
        );
    }

    private selectFromTo (from: SelectedCellCoordination, to: SelectedCellCoordination): void {
        for (let i = from.x; i <= to.x; i++) {
            for (let j = from.y; j <= to.y; j++) {
                if (this.columns[i].isEditable) {
                    this.selectedCellsState[j][i] = CellStatus.Selected;
                }
            }
        }
        this.addNewPointer(this.pointerCell);
    }

    private fittingFromTo (from: SelectedCellCoordination, to: SelectedCellCoordination): void {
        for (let i = from.x; i <= to.x; i++) {
            for (let j = from.y; j <= to.y; j++) {
                if (this.columns[i].isEditable) {
                    this.selectedCellsState[j][i] = CellStatus.Fitting;
                }
            }
        }

        this.addNewPointer(this.pointerCell);
    }

    private unselectAll (): void {
        for (let i = this.FIRST_EDITABLE_ROW; i <= this.LAST_EDITABLE_ROW; i++) {
            for (let j = this.FIRST_EDITABLE_COL; j <= this.LAST_EDITABLE_COL; j++) {
                this.selectedCellsState[i][j] = CellStatus.Unselect;
            }
        }
    }

    private addNewPointer (newPointer: SelectedCellCoordination): void {
        if (this.columns[newPointer.x].isEditable) {
            this.selectedCellsState[newPointer.y][newPointer.x] = CellStatus.Pointer;
        }
    }

    private getFromToByMouseEvent (mouseUp: MouseEvent, mouseDown: MouseEvent): [SelectedCellCoordination, SelectedCellCoordination] {
        // tslint:disable-next-line: prefer-const
        let from: Partial<SelectedCellCoordination> = {};
        // tslint:disable-next-line: prefer-const
        let to: Partial<SelectedCellCoordination> = {};

        if (mouseDown.colId <= mouseUp.colId) {
            from.x = mouseDown.colId;
            to.x = mouseUp.colId;
        } else {
            from.x = mouseUp.colId;
            to.x = mouseDown.colId;
        }

        if (mouseDown.rowId <= mouseUp.rowId) {
            from.y = mouseDown.rowId;
            to.y = mouseUp.rowId;
        } else {
            from.y = mouseUp.rowId;
            to.y = mouseDown.rowId;
        }

        return [from as SelectedCellCoordination, to as SelectedCellCoordination];
    }

    private enableEditingPointigCell (): void {
        if (this.pointerCell) {
            if (this.columns[this.pointerCell.x].isEditable) {
                this.selectedCellsState[this.pointerCell.y][this.pointerCell.x] = CellStatus.Editing;
                this.isEditingCell = true;
            } else {
                this.disableEditingMessageShow();
            }
        }
    }

    private upPointer (): void {
        this.pointerCell.y = (this.pointerCell.y > 0) ? this.pointerCell.y - 1 : this.pointerCell.y;

        this.unselectAll();
        this.addNewPointer(this.pointerCell);
    }

    private downPointer (): void {
        this.pointerCell.y = (this.pointerCell.y < this.LAST_EDITABLE_ROW) ? this.pointerCell.y + 1 : this.pointerCell.y;
        this.unselectAll();
        this.addNewPointer(this.pointerCell);
    }

    private leftPointer (): void {
        do {
            this.pointerCell.x = (this.pointerCell.x > 0) ? this.pointerCell.x - 1 : this.LAST_EDITABLE_COL;
        } while (!this.columns[this.pointerCell.x].isEditable);

        this.unselectAll();
        this.addNewPointer(this.pointerCell);
    }

    private rightPointer (): void {
        do {
            this.pointerCell.x = (this.pointerCell.x < this.LAST_EDITABLE_COL) ? this.pointerCell.x + 1 : this.FIRST_EDITABLE_COL;
        } while (!this.columns[this.pointerCell.x].isEditable);
        this.unselectAll();
        this.addNewPointer(this.pointerCell);
    }

    private onEscapeTap (): void {
        this.selectedCellsState[this.pointerCell.y][this.pointerCell.x] = CellStatus.Pointer;
        this.isEditingCell = false;
    }

    private clearDataInSelectedCells (): void {
        for (let i = 0; i < this.dataSource.length; i++) {
            for (let j = 0; j < this.displayedColumns.length; j++) {
                if (
                    this.selectedCellsState[i][j] === CellStatus.Selected ||
                    this.selectedCellsState[i][j] === CellStatus.Pointer
                ) {
                    if (this.columns[j].isEditable) {
                        this.dataSource[i][this.displayedColumns[j]] = '';
                    }
                }
            }
        }
    }

    private disableEditingMessageShow (): void {
        this._snackBar.open(this.disableEditingMessage, 'Ok', {
            duration: 2000,
        });
    }

}
