export interface SelectedCellCoordination {
    x: number;
    y: number;
}
