import { BrowserModule  } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule       } from '@angular/core';
import { MatSnackBarModule, MatTableModule, MatFormFieldModule, MatInputModule } from '@angular/material';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FocusOnShowDirective } from './autofocus';
import { TableSheetsComponent } from './components/table-sheets/table-sheets.component';

@NgModule({
    declarations: [
        AppComponent,
        FocusOnShowDirective,
        TableSheetsComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        FormsModule,
        AppRoutingModule,

        MatFormFieldModule,
        MatInputModule,
        MatTableModule,
        MatSnackBarModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
