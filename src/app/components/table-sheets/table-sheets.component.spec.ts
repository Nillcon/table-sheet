import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableSheetsComponent } from './table-sheets.component';

describe('TableSheetsComponent', () => {
  let component: TableSheetsComponent;
  let fixture: ComponentFixture<TableSheetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableSheetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableSheetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
