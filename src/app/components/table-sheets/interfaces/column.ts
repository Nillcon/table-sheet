import { CellType } from '../shared/cell-type.enum';

export interface Column {
    name: string;
    isEditable: boolean;
    dataType: CellType;
}
