export enum CellStatus {
    Unselect,
    Fitting,
    Selected,
    Pointer,
    Editing
}
